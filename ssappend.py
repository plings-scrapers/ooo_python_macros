#!/usr/bin/python
#
# Concatenate several spreadsheets
#
# Copyright (C) 2010 Ben Webb <bjwebb67@googlemail.com>
# Based on:
#   SSConverter
#   http://www.linuxjournal.com/content/convert-spreadsheets-csv-files-python-and-pyuno
# Which, in turn is based on:
#   PyODConverter (Python OpenDocument Converter) v1.0.0 - 2008-05-05
#   Copyright (C) 2008 Mirko Nasato <mirko@artofsolving.com>
#
# Licensed under the GNU LGPL v2.1 - or any later version.
# http://www.gnu.org/licenses/lgpl-2.1.html
#

import os
import re
import ooutils

import uno
from com.sun.star.task import ErrorCodeIOException
from com.sun.star.sheet.CellInsertMode import DOWN
from com.sun.star.table.CellContentType import VALUE

class SSAppender:
    def __init__(self, oorunner=None):
        self.desktop  = None
        self.oorunner = None
        
    def append(self, inputFiles, outputFile, somebool):
        if not self.desktop:
            if not self.oorunner:
                self.oorunner = ooutils.OORunner()
            self.desktop = self.oorunner.connect()
        
        masterdoc = None
        mastercon = None
        mastersheets = None
        first = True
        pos = []
        for inputFile in inputFiles:
            print
            print inputFile
            inputUrl   = uno.systemPathToFileUrl(os.path.abspath(inputFile))
            document  = self.desktop.loadComponentFromURL(inputUrl, "_blank", 0, ooutils.oo_properties())
            if first:
                masterdoc = document
                mastercon = masterdoc.getCurrentController()
                mastersheets = masterdoc.getSheets()
                
                j=0
                while j < mastersheets.getCount():
                    mastersheet = mastersheets.getByIndex(j)
                    i=0
                    while True:
                        cell = mastersheet.getCellByPosition(0,i)
                        print cell.getString()
                        if cell.getString() == "" and mastersheet.getCellByPosition(0,i+1).getString() == "" and mastersheet.getCellByPosition(1,i).getString() == "" and mastersheet.getCellByPosition(1,i+1).getString() == "":
                            break
                        i+=1
                    pos.append(i)
                    j+=1
                
                first = False
            else:
                controller = document.getCurrentController()
                sheets     = document.getSheets()
                dispatcher = uno.getComponentContext().ServiceManager.createInstance("com.sun.star.frame.DispatchHelper")
                i = 0
                print pos
                while i < mastersheets.getCount():
                    print i
                    """"
                    sheet = sheets.getByIndex(i)
                    controller.setActiveSheet(sheet)
                    #mastersheet = mastersheets.getByIndex(i)
                    
                    #cellRange = sheet.getCellRangeByPosition(0,0,10,10)
                    dispatcher.executeDispatch(controller, ".uno:SelectAll", "", 0, ())
                    cellRange = document.getCurrentSelection()
                    
                    #dispatcher.executeDispatch(controller, ".uno:Copy", "", 0, ())
                    
                    mastersheets.insertNewByName("test"+str(i), i)
                    mastersheet = mastersheets.getByIndex(i)
                    mastercon.setActiveSheet(mastersheet)
                    
                    #dispatcher.executeDispatch(mastercon, ".uno:GoToCell", "", 0, ("ToPoint", "A1"))
                    #dispatcher.executeDispatch(mastercon, ".uno:Paste", "", 0, ())
                    
                    print cellRange
                    mastersheet.insertCells(cellRange, DOWN)
                    """
                    
                    sheet = sheets.getByIndex(i)
                    controller.setActiveSheet(sheet)
                    mastersheet = mastersheets.getByIndex(i)
                    mastercon.setActiveSheet(mastersheet)
                    
                    pos[i] += 3
		    mastersheet.getCellByPosition(1, pos[i]).setString(inputFile)
                    j= 1
                    while True:
                        k=0
                        cell = sheet.getCellByPosition(0,j)
                        #print cell.getString()
                        if cell.getString() == ""  and sheet.getCellByPosition(0,j+1).getString() == "" and sheet.getCellByPosition(1,j).getString() == "" and sheet.getCellByPosition(1,j+1).getString() == "":
                            break
                        while k<30:
                            cell = sheet.getCellByPosition(k,j)
                            #print k,j
                            #print k,pos[i]
                            mcell = mastersheet.getCellByPosition(k, j+pos[i])
                            if cell.getType() == VALUE:
                                mcell.setValue(cell.getValue())
                            else:
                                mcell.setString(cell.getString())
                            k+=1
                        """mcell = mastersheet.getCellByPosition(0, j+pos[i])
                        row = sheet.getCellRangeByPosition(0,j,10,j)
                        mastersheet.copyRange(mcell.CellAddress, row.RangeAddress)
                        print row.RangeAddress"""
                        j+=1
                    pos[i] += j
                    i += 1
                if document: document.close(True)            
        
        outputUrl = uno.systemPathToFileUrl(os.path.abspath(outputFile))
        masterdoc.storeToURL(outputUrl, ooutils.oo_properties())
        if masterdoc: masterdoc.close(True)

if __name__ == "__main__":
    from sys import argv
    from os.path import isfile

    """if len(argv) == 2  and  argv[1] == '--shutdown':
        ooutils.oo_shutdown_if_running()
    else:
        if len(argv) < 3  or  len(argv) % 2 != 1:
            print "USAGE:"
            print "  python %s INPUT-FILE[:SHEET] OUTPUT-FILE ..." % argv[0]
            print "OR"
            print "  python %s --shutdown" % argv[0]
            exit(255)"""

    try:
        i = 1
        appender = SSAppender()

        args = []
        while i+1 < len(argv):
            args.append(argv[i])
            i += 1
        appender.append(args, argv[len(argv)-1], True)
        i += 2

    except ErrorCodeIOException, exception:
        print "ERROR! ErrorCodeIOException %d" % exception.ErrCode
        exit(1)
